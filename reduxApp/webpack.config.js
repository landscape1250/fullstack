const path = require('path');

module.exports = {
    entry: './src/app.js',
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'public')
    },

    module: {
        rules: [
            {
                test: /\.js$/, // include .js files
                enforce: "pre", // preload the jshint loader
                exclude: /node_modules/, // exclude any and all files in the node_modules folder
                use: [
                    {
                        loader: 'babel-loader'

                    }
                ],
                // use: [
                //     {
                //         loader: 'babel-loader',
                //         query: {
                //             presets: [
                //                 'react',
                //                 'es2015',
                //                 'stage-1',
                //             ]
                //         },
                //         options: {
                //             camelcase: true,
                //             emitErrors: false,
                //             failOnHint: false
                //         }
                //     }
                // ]
            }
        ]
    }
};