'use strict'

require('dotenv').config();
const port = process.env.APP_PORT || 3000;
const express = require('express');
const app = express();
const path = require('path');

// Middleware to define folder for static files
app.use(express.static('public'));

app.get('/', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'public', 'index.html'));
});


app.listen(port, () => {
    console.log('Web-server is listening on port ' + port);
});

